import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HospitaisComponent } from './hospitais.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HospitaisComponent]
})
export class HospitaisModule { }
