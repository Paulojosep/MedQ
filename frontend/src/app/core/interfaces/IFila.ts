export interface IFila {
    Id: number;
    QtdPessoas: number;
    Status: string;
    DataCadastro: Date;
    TipoAtendimentoId: number;
    EstabelecimentoId: number;
    EstabelecidadeId: number;
}