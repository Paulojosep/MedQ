export interface ISocio {
    Id: number;
    Nome: string;
    CPF: string;
    UF: string;
    Sexo: string;
    Email: string;
    Senha: string;
    Endereco: string;
    Complemento: string;
    Cidade: string;
    Bairro: string;
    DataCadastro: Date;
    Image: string;
    CodigoVerificacao: string;
    IdGoogle: string;
    Tipo: string;
}