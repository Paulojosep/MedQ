export class ICalendario {
    Id: number;
    Dia: string;
    HoraInicio: Date;
    HoraFim: Date;
    EstabelecimentoId: number;
}