export interface IMinhasConsulta {
    Id: number;
    Titulo: string;
    Resumo: string;
    Texto: string;
    Pedido: string;
    Senha: string;
    Profissional: string;
    Data: Date;
    Hora: Date;
    Status: string;
    Finished: string;
    ConsultaId: number;
    SocioId: number;
}