export interface IMedico {
    Id: number;
    Nome: string;
    CPF: string;
    EspecialidadeId: number;
    EstabelecimentoId: number;
}