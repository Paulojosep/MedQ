export interface IAgendamentoDisponiveis {
    Id: number;
    Data: Date;
    Hora: Date;
    Disponibilidade: number;
    MedicoId: number;
    EstabelecimentoId: number;
};