export interface ITelefone {
    Id: number;
    DDD: string;
    Numero: string;
    EstabelecimentoId: number;
    SocioId: number;
}